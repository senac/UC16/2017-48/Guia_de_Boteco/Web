# Guia de Boteco

## Objetivo

O projeto de conclusão do curso de Técnico em Informática será definido pelo grupo com o tema de cadastro de Botecos, onde iremos criar um aplicativo via android studio que irá apresentar todos os botecos e pratos cadastrado pelo estabelecimentos participantes, Além disso será criado o site onde além de ter as mesmas funções do aplicativo, será por ele que o estabelecimento ira cadastrar os pratos e divulgar seu estabelecimento e suas especialidades.

## Ferramentas

No aplicativo android utilizamos a ferramenta Android Studio, para o banco de dados do mesmo utilizamos o SQLITE.
Para o desenvolvimento web utilizamos NetBeans, para o desenvolvimento, para o banco de dados foi utilizado o MYSQL.


## Linguagem

    * Java
    * Html
    * BootStrap


## O link do projeto é:

* [Guia de Boteco](https://guiadeboteco.000webhostapp.com/)


## Autores
    * Diego Martins
    * Marcelo Bremenkamp
    * Vilcemar do Nascimento Motta
    
    
## Built com

* [Maven](https://maven.apache.org/) - Dependency Management


## Agradecimento

Certamente estes parágrafos não irão atender a todas as pessoas que fizeram parte dessa importante fase de nossas vidas. Portanto, desde já peço desculpas àquelas que não estão presentes entre essas palavras, mas elas podem estar certas que fazem parte dos nossos pensamentos e de nossa gratidão. 
Agradeço a Deus e ao campo docente da escola Senac pela sabedoria com que nos guiou nesta trajetória.
Aos meus colegas de sala.
Gostaria de deixar registrado também, o meu reconhecimento as nossas famílias, pois acreditamos que sem o apoio deles seria muito difícil vencer esse desafio. 
Enfim, a todos os que por algum motivo contribuíram para a realização deste curso.


    
