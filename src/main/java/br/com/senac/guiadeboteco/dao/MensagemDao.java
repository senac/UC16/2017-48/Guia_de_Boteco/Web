
package br.com.senac.guiadeboteco.dao;

import br.com.senac.guiadeboteco.model.MensagemPost;


public class MensagemDao extends DAO<MensagemPost>{
    
    public MensagemDao() {
        super(MensagemPost.class);
    }
    
}
