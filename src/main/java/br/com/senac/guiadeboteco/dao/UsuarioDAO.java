
package br.com.senac.guiadeboteco.dao;

import br.com.senac.guiadeboteco.model.Usuario;
import java.util.List;
import javax.persistence.Query;


public class UsuarioDAO extends DAO<Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }

    public List<Usuario> findByNome(String nome) {
         this.em = JPAUtil.getEntityManager();
        List<Usuario> list;
        em.getTransaction().begin();
        Query query = em.createQuery("from Usuario u where u.nome like :Nome");
        query.setParameter("Nome", nome + "%" );
        list = query.getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }
    
    
    public Usuario findByLogin(String nome) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("from Usuario u where u.nome = :Nome");
        query.setParameter("Nome", nome );
        Usuario usuario = (Usuario)query.getSingleResult() ; 
        em.getTransaction().commit();
        em.close();
        return usuario;
    }
    
}
