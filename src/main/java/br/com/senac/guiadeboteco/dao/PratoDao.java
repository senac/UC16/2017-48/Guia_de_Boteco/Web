
package br.com.senac.guiadeboteco.dao;

import br.com.senac.guiadeboteco.model.Pratos;


public class PratoDao extends DAO<Pratos>{
    
    public PratoDao() {
        super(Pratos.class);
    }
    
}
