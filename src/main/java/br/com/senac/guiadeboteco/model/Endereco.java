
package br.com.senac.guiadeboteco.model;

import javax.persistence.Entity;

@Entity
public class Endereco extends Entidade{
    
    private String endecampo;
    private String bairro;
    private String cidade;
    private String uf;
    private String cep;

    private String coodernadas;
    
    public Endereco() {
    }

    public String getEndecampo() {
        return endecampo;
    }

    public void setEndecampo(String endecampo) {
        this.endecampo = endecampo;
    }




    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCoodernadas() {
        return coodernadas;
    }

    public void setCoodernadas(String coodernadas) {
        this.coodernadas = coodernadas;
    }
    
   
}
