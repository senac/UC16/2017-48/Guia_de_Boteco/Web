
package br.com.senac.guiadeboteco.model;

import javax.persistence.Entity;

@Entity
public class Pratos extends Entidade{
    
    private String nome;
    
    
    private String nomeBoteco;
    
    private String imagem;
    private String descricao;
    
    private String pontucao;

    public Pratos() {
    }

    private Pratos(String imagem, String nome, String nomeBoteco, String descricao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeBoteco() {
        return nomeBoteco;
    }

    public void setNomeBoteco(String nomeBoteco) {
        this.nomeBoteco = nomeBoteco;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
    

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPontucao() {
        return pontucao;
    }

    public void setPontucao(String pontucao) {
        this.pontucao = pontucao;
    }

  

    
    
    
    
}
