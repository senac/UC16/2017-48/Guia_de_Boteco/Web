
package br.com.senac.guiadeboteco.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;



@ManagedBean
@SessionScoped
public class UploadController {
    
    private Part imagem;
    private boolean upload;
    
    public void doUpload(){
        
        try {
            InputStream in = imagem.getInputStream();
            File f = new File("D:/webguideboteca/Web/src/main/webapp/resources/upload"+imagem.getSubmittedFileName());
            f.createNewFile();
            FileOutputStream out = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int length;
            
            while((length=in.read(buffer))>0){
                
                out.write(buffer,0,length);
                
            }
            
            out.close();
            in.close();
            
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("path", f.getAbsolutePath());
            upload=true;
            
        }catch(Exception e){
            e.printStackTrace(System.out);
        }
        
        
    }

    public Part getImagem() {
        return imagem;
    }

    public void setImagem(Part imagem) {
        this.imagem = imagem;
    }

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }
    
    
}
