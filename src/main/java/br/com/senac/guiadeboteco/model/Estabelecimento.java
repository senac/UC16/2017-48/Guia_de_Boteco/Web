package br.com.senac.guiadeboteco.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Estabelecimento extends Entidade {

    private String nome;
    private String cnpj;
    private String imagem;
    private String telefone;

    private String usuario;

    private String endecampo;
    private String bairro;
    private String cidade;
    private String uf;
    private String cep;

    @Temporal(TemporalType.DATE)
    private Date dataAbertura;

    @OneToMany
    private List<Pratos> listaPratos;

    public Estabelecimento() {
        this.listaPratos = new ArrayList<>();

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEndecampo() {
        return endecampo;
    }

    public void setEndecampo(String endecampo) {
        this.endecampo = endecampo;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Date getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(Date dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public List<Pratos> getListaPratos() {
        return listaPratos;
    }

    public void setListaPratos(List<Pratos> listaPratos) {
        this.listaPratos = listaPratos;
    }

}
