package br.com.senac.guiadeboteco.model;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class Usuario extends Entidade {

    private String nome;
    private String senha;
    private String email;
    private String cpf;
    
    @Transient
    private String confSenha;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getConfSenha() {
        return confSenha;
    }

    public void setConfSenha(String confSenha) {
        this.confSenha = confSenha;
    }
    
    

  
}
