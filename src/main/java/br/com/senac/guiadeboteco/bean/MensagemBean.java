
package br.com.senac.guiadeboteco.bean;

import br.com.senac.guiadeboteco.dao.MensagemDao;
import br.com.senac.guiadeboteco.model.MensagemPost;
import javax.inject.Named;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;


@Named(value = "MensagemBean")
@ViewScoped
public class MensagemBean extends Bean{

    private MensagemPost mensagem;
    private MensagemDao dao;
    private List<MensagemPost> listaMensagem ;
    
    public MensagemBean() {
    }
       

    @PostConstruct
    public void init() {
        this.mensagem = new MensagemPost();
        this.dao = new MensagemDao();
        this.listaMensagem = dao.findAll() ; 
    }
    
    
    public void salvar(){
        
        if(this.mensagem.getId() == 0 ){
            dao.save(mensagem);
            addMessageInfo("Salvo com sucesso.");
        }else{
            dao.update(mensagem);
            addMessageInfo("Atualizado com sucesso.");
        }
         this.listaMensagem = dao.findAll() ; 
    }
    
    public void novo(){
        this.mensagem = new MensagemPost();
    }
    
    public void deletar(MensagemPost mensagem){
        this.dao.delete(mensagem);
        addMessageInfo("Mensagem deletado com sucesso.");
        this.listaMensagem = dao.findAll();
    }

    public MensagemPost getMensagem() {
        return mensagem;
    }

    public void setMensagem(MensagemPost mensagem) {
        this.mensagem = mensagem;
    }

     public List<MensagemPost> getListaMensagem() {
        return listaMensagem;
    }

    public void setListaMensagem(List<MensagemPost> listaMensagem) {
        this.listaMensagem = listaMensagem;
    }
    
    
    
}
