
package br.com.senac.guiadeboteco.bean;

import br.com.senac.guiadeboteco.dao.PratoDao;
import br.com.senac.guiadeboteco.model.Pratos;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named(value = "pratosbean")
@ViewScoped
public class PratosBean extends Bean{

private Pratos prato;
private PratoDao dao;
private List<Pratos> listPratos;

    public PratosBean() {
    }

@PostConstruct
    public void init() {
        this.prato = new Pratos();
        this.dao = new PratoDao();
        this.listPratos = dao.findAll() ; 
    }
    
    
    public void salvar(){
        
        if(this.prato.getId() == 0 ){
            dao.save(prato);
            addMessageInfo("Salvo com sucesso.");
        }else{
            dao.update(prato);
            addMessageInfo("Atualizado com sucesso.");
        }
         this.listPratos = dao.findAll() ; 
    }
    
    public void novo(){
        this.prato = new Pratos();
    }
    
    public void deletar(Pratos prato){
        this.dao.delete(prato);
        addMessageInfo("Prato deletado com sucesso.");
        this.listPratos = dao.findAll();
    }
    


    public Pratos getPrato() {
        return prato;
    }

    public void setPrato(Pratos prato) {
        this.prato = prato;
    }

    public PratoDao getDao() {
        return dao;
    }

    public void setDao(PratoDao dao) {
        this.dao = dao;
    }

    public List<Pratos> getListPratos() {
        return listPratos;
    }

    public void setListPratos(List<Pratos> listPratos) {
        this.listPratos = listPratos;
    }
    
    public List<Pratos> createPratos(int size) {
        List<Pratos> list = new ArrayList<Pratos>();
        for(int i = 0 ; i < size ; i++) {
            list.add(new Pratos());
        }
         
        return list;
    }
    
 
         


}
