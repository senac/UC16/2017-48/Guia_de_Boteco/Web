
package br.com.senac.guiadeboteco.bean;

import br.com.senac.guiadeboteco.dao.EstabelecimentoDao;

import br.com.senac.guiadeboteco.model.Estabelecimento;
import br.com.senac.guiadeboteco.model.Pratos;



import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


@Named(value = "estabelecimentoBean")
@ViewScoped
public class EstabelecimentoBean extends Bean{
    
    private Estabelecimento estabelecimento;
    private EstabelecimentoDao dao;
    private List<Estabelecimento> listaEstab ;
    private List<Estabelecimento> listaBotecos;
    private Estabelecimento selectBoteco;
    private int tamanhoDalista;

    public Estabelecimento getSelectBoteco() {
        return selectBoteco;
    }

    public void setSelectBoteco(Estabelecimento selectBoteco) {
        this.selectBoteco = selectBoteco;
    }


    
    
    
    private Pratos pratos ; 

    public Pratos getPratos() {
        return pratos;
    }

    public void setPratos(Pratos pratos) {
        this.pratos = pratos;
    }
    
    
    
    public void salvarPrato(){
        
        this.estabelecimento.getListaPratos().add(pratos);
    }
    
   
    public EstabelecimentoBean(){
    
    }
    
    @ManagedProperty("#{Estabelecimento}")
    private Estabelecimento service;
    
    @PostConstruct
    public void init() {
        this.estabelecimento = new Estabelecimento();
        this.dao = new EstabelecimentoDao();
        this.listaEstab = dao.findAll() ;
    }
    

    
    public void salvar(){
        
        if(this.estabelecimento.getId() == 0 ){
            dao.save(estabelecimento);
            addMessageInfo("Salvo com sucesso.");
        }else{
            dao.update(estabelecimento);
            addMessageInfo("Atualizado com sucesso.");
        }
         this.listaEstab = dao.findAll() ; 
    }
    
    public void novo(){
        this.estabelecimento = new Estabelecimento();
    }
    
    public void novoPrato(){
        this.pratos = new Pratos();
    }
    
    public void deletar(Estabelecimento estabelecimento){
        this.dao.delete(estabelecimento);
        addMessageInfo("Estabelecimento deletado com sucesso.");
        this.listaEstab = dao.findAll();
    }

    public Estabelecimento getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(Estabelecimento estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public List<Estabelecimento> getListaEstab() {
        return listaEstab;
    }

    public void setListaEstab(List<Estabelecimento> listaEstab) {
        this.listaEstab = listaEstab;
    }
       

}
