/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.guiadeboteco.bean;

import br.com.senac.guiadeboteco.dao.UsuarioDAO;
import br.com.senac.guiadeboteco.model.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author sala304b
 */
@Named(value = "pesquisaUsuarioBean")
@ViewScoped
public class PesquisaUsuarioBean extends Bean {

    private List<Usuario> lista;
    private String nome;

    private UsuarioDAO dao;

    public PesquisaUsuarioBean() {
    }

    @PostConstruct
    public void init() {
        this.dao = new UsuarioDAO();
        this.lista = dao.findAll();
    }

    public List<Usuario> getLista() {
        return lista;
    }

    public void pesquisar() {
        if (!this.nome.equals("")) {
            this.lista = dao.findByNome(this.nome);
        } else {
            this.lista = dao.findAll();
        }
    }

    public void setLista(List<Usuario> lista) {
        this.lista = lista;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
