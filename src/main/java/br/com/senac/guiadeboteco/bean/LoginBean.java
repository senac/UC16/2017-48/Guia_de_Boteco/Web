package br.com.senac.guiadeboteco.bean;

import br.com.senac.guiadeboteco.dao.UsuarioDAO;
import br.com.senac.guiadeboteco.model.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.persistence.NoResultException;
import javax.swing.JOptionPane;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean extends Bean {

    private Usuario usuario;
    private UsuarioDAO dao;

    private String nome;
    private String senha;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    private boolean confirmação = false;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LoginBean() {
        this.dao = new UsuarioDAO();
    }

    public boolean isUsuarioLogado() {
        return this.usuario != null;
    }

    public String comparar(String senha, String confSenha) {

        if (senha == null ? confSenha == null : senha.equals(confSenha)) {
            //bean.salvar();
            JOptionPane.showMessageDialog(null, "Senha Salva Com Sucesso!");
        }
        JOptionPane.showMessageDialog(null, "Senha!");

        return null;

    }

    public String login() {

        try {
            this.usuario = dao.findByLogin(nome);
            if (usuario.getSenha().equals(senha)) {
                return "/estabelecimento/gerenciarestab.xhtml?faces-redirect=true";
            } else {
                addMessageError("Usuario ou senha invalido !!!!!");
                return null;
            }
        } catch (NoResultException ex) {
            addMessageError("Usuário não cadastrado!");
            return null;
        }

    }

}
