package br.com.senac.guiadeboteco.bean;


import br.com.senac.guiadeboteco.dao.UsuarioDAO;
import br.com.senac.guiadeboteco.model.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "usuarioBean")
@ViewScoped
public class UsuarioBean extends Bean {

    private Usuario usuario;
    private UsuarioDAO dao;
    
    private String confSenha;

    public UsuarioBean() {

    }

    @PostConstruct
    public void init() {
        this.usuario = new Usuario();
        this.dao = new UsuarioDAO();
       
    }
    
    
    public String salvar(){
        
        if(this.usuario.getId() == 0 ){
            dao.save(usuario);
            addMessageInfo("Salvo com sucesso.");
        }else{
            dao.update(usuario);
            addMessageInfo("Atualizado com sucesso.");
        }
        
        return "/index?faces-redirect=true" ; 
       
    }
    
    public void novo(){
        this.usuario = new Usuario();
    }
    
    public void deletar(Usuario usuario){
        this.dao.delete(usuario);
        addMessageInfo("Usuario deletado com sucesso.");
       
    }
    
    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

  

    public UsuarioDAO getDao() {
        return dao;
    }

    public void setDao(UsuarioDAO dao) {
        this.dao = dao;
    }

    public String getConfSenha() {
        return confSenha;
    }

    public void setConfSenha(String confSenha) {
        this.confSenha = confSenha;
    }


    


    

}
